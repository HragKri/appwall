"use strict";

Template.form.events({
    'submit .add-new-post': function (event) {
        event.preventDefault();

        var postName = event.currentTarget.children[0].value[0];
        
        var postImage = event.currentTarget.children[5].files[0];
        
        var postMessage = event.currentTarget.children[2].value;

        Collections.Images.insert(postImage, function (error, fileObject) {
            if (error) {
                //to-do: inform the user that it failed
            } else {
               Collections.Post.insert({
               name: postName,
                createdAt: new Data(),
                   message: postMessage,
                   ImageID: postImage
               
               });
                $('.grid').masonry('reloadItems');
            }
        });
    }
});

//Template.todos.events({
   //'click.delete-task': function (event) {
    //Collections.Todo.remove({_id: this._id});   
  // }
//});